﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.Message;

namespace EnergyDataCollectionService.RabbitMQEventBus
{
    public class FunctionProvider : RabbitMQFunctionProviderBase
    {
        readonly ILogger<FunctionProvider> _logger;

        public FunctionProvider(ILogger<FunctionProvider> logger)
        {
            _logger = logger;
        }

        protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>> InializeFunctionsWithKeys()
        {
            var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
            dictionary.Add(nameof(HandleIncomingData), HandleIncomingData);

            return dictionary;
        }
            
        public RabbitMQMessage HandleIncomingData(object o, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            return null;
        }
    }
}
