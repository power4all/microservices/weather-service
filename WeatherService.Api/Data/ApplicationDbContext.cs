﻿using System;
using EnergyDataCollectionService.Models.Entity;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace EnergyDataCollectionService.Data
{
	public partial class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}

		public DbSet<SolarData> SolarData { get; set; }

		public DbSet<WindData> WindData { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}