using System;
using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Helpers;
using EnergyDataCollectionService.Logic.Collectors;
using EnergyDataCollectionService.RabbitMQEventBus;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RabbitMQEventbus.Extension.DependencyInjection;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.RabbitMQEventbusConfiguration;

namespace EnergyDataCollectionService
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            _env = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseMySql(
                    Configuration.GetConnectionString("MySql"),
                    new MySqlServerVersion(new Version(8, 0, 23)),
                mySqlOptions =>
                {
                    mySqlOptions.EnableRetryOnFailure(
                        maxRetryCount: 2);
                });
            });

            services.Configure<RabbitMQEventbusConfiguration>(Configuration);
            services.AddOptions<RabbitMQEventbusConfiguration>();
            services.AddSingleton<IRabbitMQFunctionProvider>(s => { return new FunctionProvider(s.GetService<ILogger<FunctionProvider>>()); });
            services.AddRabbitMQEventbus(_env);

            services.Configure<RouteOptions>(o => o.LowercaseUrls = true);
            services.AddControllers(o =>
            {
                o.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WeatherService.Api", Version = "v1" });
            });

            services.AddScoped<InitCollectors>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WeatherService.Api v1"));
            }

            app.UseRabbitMQEventBus();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var initProducers = serviceProvider.GetService<InitCollectors>();
        }
    }
}
