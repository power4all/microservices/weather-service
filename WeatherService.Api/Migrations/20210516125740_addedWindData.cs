﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EnergyDataCollectionService.Migrations
{
    public partial class addedWindData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "windSpeed",
                table: "WindData",
                newName: "WindSpeed");

            migrationBuilder.AddColumn<double>(
                name: "Temprature",
                table: "WindData",
                type: "double",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Temprature",
                table: "WindData");

            migrationBuilder.RenameColumn(
                name: "WindSpeed",
                table: "WindData",
                newName: "windSpeed");
        }
    }
}
