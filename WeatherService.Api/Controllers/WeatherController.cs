﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;

namespace EnergyDataCollectionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IRabbitMQEventbus _eventbus;

        public WeatherController(IRabbitMQEventbus eventbus)
        {
            _eventbus = eventbus;
        }

        [HttpGet]
        public Task SendMessage()
        {
            var message = "test";

            var json = JsonConvert.SerializeObject(message);

            _eventbus.Publish(new RabbitMQMessage(new MessageDestination("WeatherExchange", "weather.data"), json));

            return Task.CompletedTask;
        }
    }
}
