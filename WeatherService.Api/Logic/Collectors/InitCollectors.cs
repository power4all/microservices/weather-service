﻿using Microsoft.Extensions.DependencyInjection;
using RabbitMQEventbus.Extension.Eventbus;

namespace EnergyDataCollectionService.Logic.Collectors
{
    public class InitCollectors
    {
        private readonly IRabbitMQEventbus _eventbus;
        private readonly IServiceScopeFactory _scopeFactory;

        public InitCollectors(IRabbitMQEventbus eventbus, IServiceScopeFactory scopeFactory)
        {
            _eventbus = eventbus;
            _scopeFactory = scopeFactory;

            CreateCollectors();
        }

        private void CreateCollectors()
        {
            CreateSolarCollector();
            CreateWindCollector();
        }

        private void CreateSolarCollector()
        {
            var solarCollector = new SolarCollector(_eventbus, _scopeFactory);
        }

        private void CreateWindCollector()
		{
            var windCollector = new WindCollector(_eventbus, _scopeFactory);
		}
    }
}
