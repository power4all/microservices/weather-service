﻿using System;
using System.Collections.Generic;
using System.Timers;
using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Entity;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using WeatherService.Api.Models.DTO.Solar;

namespace EnergyDataCollectionService.Logic.Collectors
{
    public class SolarCollector
    {
        private readonly IRabbitMQEventbus _eventbus;
        private readonly IServiceScopeFactory _scopeFactory;

        public SolarCollector(IRabbitMQEventbus eventbus, IServiceScopeFactory scopeFactory)
        {
            _eventbus = eventbus;
            _scopeFactory = scopeFactory;

            InitTimer();
        }

        private void InitTimer()
        {

            Timer t = new Timer();
            t.Interval = 590000; // In milliseconds | every 10 minutes
            t.AutoReset = true; // False stops it from repeating
            t.Elapsed += new ElapsedEventHandler(PublishSolarDataAsync);
            t.Start();
        }

        private async void PublishSolarDataAsync(object sender, ElapsedEventArgs e)
        {
            var url = "https://api.tomorrow.io/v4/timelines"
              .SetQueryParams(new
              {
                  // 6f0bc7yEotESa9MfVmuuqfZZ4GmMr6dr f0vUAEhCQ0rIUZzqO991gAXgwhZxzUnc
                  apikey = "6f0bc7yEotESa9MfVmuuqfZZ4GmMr6dr",
                  location = "4.937386794198567, 52.40016844635332",
                  fields = "solarGHI",
                  units = "metric",
                  timesteps = "1m",
                  startTime = DateTime.Now.AddMinutes(-9),
                  endTime = DateTime.Now,
                  timezone = "Europe/Amsterdam",
              });

            var response = await url.GetJsonAsync<SolarResponse>();

            var solarMessage = new SolarMessage
            {
                SolarIntervals = new List<SolarData>(),
                StartTime = response.Data.Timelines[0].StartTime,
                EndTime = response.Data.Timelines[0].EndTime,
            };

            foreach (var interval in response.Data.Timelines[0].Intervals)
            {
                double solarGHI = interval.Values.SolarGHI;

                var solarData = new SolarData
                {
                    GHI = solarGHI,
                    Date = interval.StartTime
                };

                using (var scope = _scopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    context.SolarData.Add(solarData);
                    context.SaveChanges();
                }

                solarMessage.SolarIntervals.Add(solarData);
            }

            var json = JsonConvert.SerializeObject(solarMessage);

            _eventbus.Publish(new RabbitMQMessage(new MessageDestination("WeatherExchange", "weather.solar"), json));

            Console.WriteLine("Message sent");
        }
    }
}
