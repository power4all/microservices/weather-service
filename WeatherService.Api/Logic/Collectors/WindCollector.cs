﻿using System;
using System.Collections.Generic;
using System.Timers;
using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.DTO.Wind;
using EnergyDataCollectionService.Models.Entity;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;

namespace EnergyDataCollectionService.Logic.Collectors
{
	public class WindCollector
	{
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;

        public WindCollector(IRabbitMQEventbus eventbus, IServiceScopeFactory scopeFactory)
        {
            _eventbus = eventbus;
            _scopeFactory = scopeFactory;

            InitTimer();
        }

        private void InitTimer()
        {

            Timer t = new Timer();
            t.Interval = 600000; // In milliseconds | every 10 minutes
            t.AutoReset = true; // False stops it from repeating
            t.Elapsed += new ElapsedEventHandler(PublishWindDataAsync);
            t.Start();
        }

        private async void PublishWindDataAsync(object sender, ElapsedEventArgs e)
        {
            var url = "https://api.tomorrow.io/v4/timelines"
              .SetQueryParams(new
              {
                  //"f0vUAEhCQ0rIUZzqO991gAXgwhZxzUnc", 
                  apikey = "g0V7tfHYCc0DaPevS7us7G6Z2adXT7Vs",
                  location = "4.937386794198567, 52.40016844635332",
                  fields = "windSpeed",
                  units = "metric",
                  timesteps = "1m",
                  startTime = DateTime.Now.AddMinutes(-9),
                  endTime = DateTime.Now,
                  timezone = "Europe/Amsterdam",
              });

            var response = await url.GetJsonAsync<WindResponse>();

            var windMessage = new WindMessage
            {
                WindSpeedIntervals = new List<WindData>(),
                StartTime = response.Data.Timelines[0].StartTime,
                EndTime = response.Data.Timelines[0].EndTime,
            };

            foreach (var interval in response.Data.Timelines[0].Intervals)
            {
                double WindSpeed = interval.Values.WindSpeed;
                double temprature = interval.Values.temprature;

                var windData = new WindData
                {
                    WindSpeed = WindSpeed,
                    Temprature = temprature,
                    Date = interval.StartTime,
                };

                using (var scope = _scopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    context.WindData.Add(windData);
                    context.SaveChanges();
                }

                windMessage.WindSpeedIntervals.Add(windData);
            }

            var json = JsonConvert.SerializeObject(windMessage);

            _eventbus.Publish(new RabbitMQMessage(new MessageDestination("WeatherExchange", "weather.wind"), json));

            Console.WriteLine("Message sent");
        }
    }
}
