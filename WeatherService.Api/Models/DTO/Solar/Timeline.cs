﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherService.Api.Models.DTO.Solar
{
    public class Timeline
    {
        public string Timestep { get; init; }
        public DateTime StartTime { get; init; }
        public DateTime EndTime { get; init; }
        public List<Interval> Intervals { get; init; }
    }
}
