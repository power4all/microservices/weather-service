﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherService.Api.Models.DTO.Solar
{
    public class SolarResponse
    {
        public Data Data { get; init; }
    }
}
