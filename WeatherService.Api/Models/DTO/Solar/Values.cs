﻿namespace WeatherService.Api.Models.DTO.Solar
{
    public class Values
    {
        public double SolarGHI { get; init; }
    }
}