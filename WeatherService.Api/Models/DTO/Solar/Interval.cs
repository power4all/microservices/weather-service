﻿using System;

namespace WeatherService.Api.Models.DTO.Solar
{
    public class Interval
    {
        public DateTime StartTime { get; init; }
        public Values Values { get; init; }
    }
}