﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherService.Api.Models.DTO.Solar
{
    public class Data
    {
        public List<Timeline> Timelines { get; init; }
    }
}
