﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Models.DTO.Wind
{
	public class Interval
	{
		public DateTime StartTime { get; init; }
		public Values Values { get; init; }
	}
}
