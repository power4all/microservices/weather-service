﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Models.DTO.Wind
{
	public class Values
	{
		public double WindSpeed { get; init; }
		public double temprature { get; init; }
	}
}
