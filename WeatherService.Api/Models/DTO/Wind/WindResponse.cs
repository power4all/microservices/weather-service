﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Models.DTO.Wind
{
	public class WindResponse
	{
		public Data Data { get; init; }
	}
}
