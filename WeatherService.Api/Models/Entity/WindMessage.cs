﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Models.Entity
{
	public class WindMessage
	{
		public List<WindData> WindSpeedIntervals { get; init; }
		public DateTime StartTime { get; init; }
		public DateTime EndTime { get; init; }
	}
}
