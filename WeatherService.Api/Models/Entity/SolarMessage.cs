﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Models.Entity
{
    public class SolarMessage
    {
        public List<SolarData> SolarIntervals { get; init; }
        public DateTime StartTime { get; init; }
        public DateTime EndTime { get; init; }
    }
}
